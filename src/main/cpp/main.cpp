#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>

#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"

#include "proto/measurements.pb.h"
#include "Ameasurements.hh"

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <avro/Stream.hh>
#include <avro/Decoder.hh>

using namespace std;
using boost::asio::ip::tcp;

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<Dataset> datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){
    string line;
    getline(stream, line);
    int messageSize = stoi(line);
    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);
    std::unique_ptr<avro::InputStream> in = avro::memoryInputStream((uint8_t*)buffer, messageSize);
    avro::DecoderPtr d = avro::binaryDecoder();
    d->init(*in);
    Message message;
    avro::decode(*d, message);
    vector<ADataset> dataset_array;
    for(int i=0; i<message.dataset.size(); i++){
        ADataset dataset = message.dataset[i];
        ARecords records = dataset.Arecord;
        double download_sum = 0;
        double upload_sum = 0;
        double ping_sum = 0;
        for(int j=0; j<records.download.size(); j++){
            download_sum += records.download[j];
            upload_sum += records.upload[j];
            ping_sum += records.ping[j];
        }
        int size = records.download.size();
        records.download = {download_sum/size};
        records.upload = {upload_sum/size};
        records.ping = {ping_sum/size};
        dataset.Arecord = records;
        dataset_array.push_back(dataset);
    }
    message.dataset = dataset_array;
    std::auto_ptr<avro::OutputStream> output = avro::memoryOutputStream();
    avro::EncoderPtr e = avro::binaryEncoder();
    e->init(*output);
    avro::encode(*e, message);
    std::auto_ptr<avro::InputStream> i = avro::memoryInputStream(*output);
    avro::StreamReader c = avro::StreamReader(i.operator*());
    vector<uint8_t> buf;
    while(c.hasMore()){
        buf.push_back(c.read());
    }
    for(uint8_t b : buf){
        stream << b;
    }


}

void processProtobuf(tcp::iostream& stream){

    string line;
    getline(stream, line);
    int messageSize = stoi(line);
    char *buffer = new char[messageSize];
    stream.read(buffer, messageSize);
    esw::PMessage message;

    message.ParseFromArray((void*)buffer, messageSize);
    int size = message.messagearray_size();
    esw::PDataset dataset_array[size];
    for(int i=0; i < size; i++){
        esw::PDataset dataset = message.messagearray(i);
        esw::PRecords* records = dataset.release_records();
        double download_sum = 0;
        double upload_sum = 0;
        double ping_sum = 0;
        for(int j=0; j<records->download_size(); j++){
            download_sum += records->download(j);
            upload_sum += records->upload(j);
            ping_sum += records->ping(j);
        }
        double download_size = records->download_size();
        records->clear_download();
        records->add_download(download_sum/download_size);
        records->clear_upload();
        records->add_upload(upload_sum/download_size);
        records->clear_ping();
        records->add_ping(ping_sum/download_size);
        dataset.set_allocated_records(records);
        dataset_array[i] = dataset;
    }
    esw::PMessage new_message;
    for(int i=0; i<size; i++){
        esw::PDataset* d = new_message.add_messagearray();
        d->set_allocated_records(dataset_array[i].release_records());
        d->set_allocated_info(dataset_array[i].release_info());
    }
    std::string output;
    new_message.SerializeToString(&output);
    stream << output;
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }



    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
