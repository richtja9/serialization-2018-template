/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef _HOME_JARICHTE_DOWNLOADS_ESW_SERIALIZATION_SERIALIZATION_2018_TEMPLATE_SRC_MAIN_CPP_AMEASUREMENTS_HH_332550728__H_
#define _HOME_JARICHTE_DOWNLOADS_ESW_SERIALIZATION_SERIALIZATION_2018_TEMPLATE_SRC_MAIN_CPP_AMEASUREMENTS_HH_332550728__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

struct AInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct ARecords {
    std::vector<double > download;
    std::vector<double > upload;
    std::vector<double > ping;
    ARecords() :
        download(std::vector<double >()),
        upload(std::vector<double >()),
        ping(std::vector<double >())
        { }
};

struct ADataset {
    AInfo Ainfo;
    ARecords Arecord;
    ADataset() :
        Ainfo(AInfo()),
        Arecord(ARecords())
        { }
};

struct Message {
    std::vector<ADataset > dataset;
    Message() :
        dataset(std::vector<ADataset >())
        { }
};

namespace avro {
template<> struct codec_traits<AInfo> {
    static void encode(Encoder& e, const AInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, AInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<ARecords> {
    static void encode(Encoder& e, const ARecords& v) {
        avro::encode(e, v.download);
        avro::encode(e, v.upload);
        avro::encode(e, v.ping);
    }
    static void decode(Decoder& d, ARecords& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.download);
                    break;
                case 1:
                    avro::decode(d, v.upload);
                    break;
                case 2:
                    avro::decode(d, v.ping);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.download);
            avro::decode(d, v.upload);
            avro::decode(d, v.ping);
        }
    }
};

template<> struct codec_traits<ADataset> {
    static void encode(Encoder& e, const ADataset& v) {
        avro::encode(e, v.Ainfo);
        avro::encode(e, v.Arecord);
    }
    static void decode(Decoder& d, ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.Ainfo);
                    break;
                case 1:
                    avro::decode(d, v.Arecord);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.Ainfo);
            avro::decode(d, v.Arecord);
        }
    }
};

template<> struct codec_traits<Message> {
    static void encode(Encoder& e, const Message& v) {
        avro::encode(e, v.dataset);
    }
    static void decode(Decoder& d, Message& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.dataset);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.dataset);
        }
    }
};

}
#endif
