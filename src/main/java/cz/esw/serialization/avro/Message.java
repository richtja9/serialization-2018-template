/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package cz.esw.serialization.avro;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class Message extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 2832235856735061355L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Message\",\"namespace\":\"cz.esw.serialization.avro\",\"fields\":[{\"name\":\"dataset\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"ADataset\",\"fields\":[{\"name\":\"Ainfo\",\"type\":{\"type\":\"record\",\"name\":\"AInfo\",\"fields\":[{\"name\":\"id\",\"type\":\"int\"},{\"name\":\"timestamp\",\"type\":\"long\"},{\"name\":\"measurerName\",\"type\":\"string\"}]}},{\"name\":\"Arecord\",\"type\":{\"type\":\"record\",\"name\":\"ARecords\",\"fields\":[{\"name\":\"download\",\"type\":{\"type\":\"array\",\"items\":\"double\"}},{\"name\":\"upload\",\"type\":{\"type\":\"array\",\"items\":\"double\"}},{\"name\":\"ping\",\"type\":{\"type\":\"array\",\"items\":\"double\"}}]}}]}}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<Message> ENCODER =
      new BinaryMessageEncoder<Message>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<Message> DECODER =
      new BinaryMessageDecoder<Message>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<Message> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<Message> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<Message>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this Message to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a Message from a ByteBuffer. */
  public static Message fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.util.List<cz.esw.serialization.avro.ADataset> dataset;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public Message() {}

  /**
   * All-args constructor.
   * @param dataset The new value for dataset
   */
  public Message(java.util.List<cz.esw.serialization.avro.ADataset> dataset) {
    this.dataset = dataset;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return dataset;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: dataset = (java.util.List<cz.esw.serialization.avro.ADataset>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'dataset' field.
   * @return The value of the 'dataset' field.
   */
  public java.util.List<cz.esw.serialization.avro.ADataset> getDataset() {
    return dataset;
  }

  /**
   * Sets the value of the 'dataset' field.
   * @param value the value to set.
   */
  public void setDataset(java.util.List<cz.esw.serialization.avro.ADataset> value) {
    this.dataset = value;
  }

  /**
   * Creates a new Message RecordBuilder.
   * @return A new Message RecordBuilder
   */
  public static cz.esw.serialization.avro.Message.Builder newBuilder() {
    return new cz.esw.serialization.avro.Message.Builder();
  }

  /**
   * Creates a new Message RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new Message RecordBuilder
   */
  public static cz.esw.serialization.avro.Message.Builder newBuilder(cz.esw.serialization.avro.Message.Builder other) {
    return new cz.esw.serialization.avro.Message.Builder(other);
  }

  /**
   * Creates a new Message RecordBuilder by copying an existing Message instance.
   * @param other The existing instance to copy.
   * @return A new Message RecordBuilder
   */
  public static cz.esw.serialization.avro.Message.Builder newBuilder(cz.esw.serialization.avro.Message other) {
    return new cz.esw.serialization.avro.Message.Builder(other);
  }

  /**
   * RecordBuilder for Message instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Message>
    implements org.apache.avro.data.RecordBuilder<Message> {

    private java.util.List<cz.esw.serialization.avro.ADataset> dataset;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(cz.esw.serialization.avro.Message.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.dataset)) {
        this.dataset = data().deepCopy(fields()[0].schema(), other.dataset);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing Message instance
     * @param other The existing instance to copy.
     */
    private Builder(cz.esw.serialization.avro.Message other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.dataset)) {
        this.dataset = data().deepCopy(fields()[0].schema(), other.dataset);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'dataset' field.
      * @return The value.
      */
    public java.util.List<cz.esw.serialization.avro.ADataset> getDataset() {
      return dataset;
    }

    /**
      * Sets the value of the 'dataset' field.
      * @param value The value of 'dataset'.
      * @return This builder.
      */
    public cz.esw.serialization.avro.Message.Builder setDataset(java.util.List<cz.esw.serialization.avro.ADataset> value) {
      validate(fields()[0], value);
      this.dataset = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'dataset' field has been set.
      * @return True if the 'dataset' field has been set, false otherwise.
      */
    public boolean hasDataset() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'dataset' field.
      * @return This builder.
      */
    public cz.esw.serialization.avro.Message.Builder clearDataset() {
      dataset = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Message build() {
      try {
        Message record = new Message();
        record.dataset = fieldSetFlags()[0] ? this.dataset : (java.util.List<cz.esw.serialization.avro.ADataset>) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<Message>
    WRITER$ = (org.apache.avro.io.DatumWriter<Message>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<Message>
    READER$ = (org.apache.avro.io.DatumReader<Message>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
