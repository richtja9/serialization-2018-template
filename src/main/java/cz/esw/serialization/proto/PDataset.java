// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: proto/measurements.proto

package cz.esw.serialization.proto;

/**
 * Protobuf type {@code esw.PDataset}
 */
public  final class PDataset extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:esw.PDataset)
    PDatasetOrBuilder {
private static final long serialVersionUID = 0L;
  // Use PDataset.newBuilder() to construct.
  private PDataset(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private PDataset() {
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private PDataset(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            cz.esw.serialization.proto.PInfo.Builder subBuilder = null;
            if (info_ != null) {
              subBuilder = info_.toBuilder();
            }
            info_ = input.readMessage(cz.esw.serialization.proto.PInfo.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(info_);
              info_ = subBuilder.buildPartial();
            }

            break;
          }
          case 18: {
            cz.esw.serialization.proto.PRecords.Builder subBuilder = null;
            if (records_ != null) {
              subBuilder = records_.toBuilder();
            }
            records_ = input.readMessage(cz.esw.serialization.proto.PRecords.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom(records_);
              records_ = subBuilder.buildPartial();
            }

            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return cz.esw.serialization.proto.Measurements.internal_static_esw_PDataset_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return cz.esw.serialization.proto.Measurements.internal_static_esw_PDataset_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            cz.esw.serialization.proto.PDataset.class, cz.esw.serialization.proto.PDataset.Builder.class);
  }

  public static final int INFO_FIELD_NUMBER = 1;
  private cz.esw.serialization.proto.PInfo info_;
  /**
   * <code>.esw.PInfo info = 1;</code>
   */
  public boolean hasInfo() {
    return info_ != null;
  }
  /**
   * <code>.esw.PInfo info = 1;</code>
   */
  public cz.esw.serialization.proto.PInfo getInfo() {
    return info_ == null ? cz.esw.serialization.proto.PInfo.getDefaultInstance() : info_;
  }
  /**
   * <code>.esw.PInfo info = 1;</code>
   */
  public cz.esw.serialization.proto.PInfoOrBuilder getInfoOrBuilder() {
    return getInfo();
  }

  public static final int RECORDS_FIELD_NUMBER = 2;
  private cz.esw.serialization.proto.PRecords records_;
  /**
   * <code>.esw.PRecords records = 2;</code>
   */
  public boolean hasRecords() {
    return records_ != null;
  }
  /**
   * <code>.esw.PRecords records = 2;</code>
   */
  public cz.esw.serialization.proto.PRecords getRecords() {
    return records_ == null ? cz.esw.serialization.proto.PRecords.getDefaultInstance() : records_;
  }
  /**
   * <code>.esw.PRecords records = 2;</code>
   */
  public cz.esw.serialization.proto.PRecordsOrBuilder getRecordsOrBuilder() {
    return getRecords();
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (info_ != null) {
      output.writeMessage(1, getInfo());
    }
    if (records_ != null) {
      output.writeMessage(2, getRecords());
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (info_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(1, getInfo());
    }
    if (records_ != null) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(2, getRecords());
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof cz.esw.serialization.proto.PDataset)) {
      return super.equals(obj);
    }
    cz.esw.serialization.proto.PDataset other = (cz.esw.serialization.proto.PDataset) obj;

    if (hasInfo() != other.hasInfo()) return false;
    if (hasInfo()) {
      if (!getInfo()
          .equals(other.getInfo())) return false;
    }
    if (hasRecords() != other.hasRecords()) return false;
    if (hasRecords()) {
      if (!getRecords()
          .equals(other.getRecords())) return false;
    }
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    if (hasInfo()) {
      hash = (37 * hash) + INFO_FIELD_NUMBER;
      hash = (53 * hash) + getInfo().hashCode();
    }
    if (hasRecords()) {
      hash = (37 * hash) + RECORDS_FIELD_NUMBER;
      hash = (53 * hash) + getRecords().hashCode();
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static cz.esw.serialization.proto.PDataset parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static cz.esw.serialization.proto.PDataset parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static cz.esw.serialization.proto.PDataset parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static cz.esw.serialization.proto.PDataset parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(cz.esw.serialization.proto.PDataset prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code esw.PDataset}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:esw.PDataset)
      cz.esw.serialization.proto.PDatasetOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return cz.esw.serialization.proto.Measurements.internal_static_esw_PDataset_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return cz.esw.serialization.proto.Measurements.internal_static_esw_PDataset_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              cz.esw.serialization.proto.PDataset.class, cz.esw.serialization.proto.PDataset.Builder.class);
    }

    // Construct using cz.esw.serialization.proto.PDataset.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      if (infoBuilder_ == null) {
        info_ = null;
      } else {
        info_ = null;
        infoBuilder_ = null;
      }
      if (recordsBuilder_ == null) {
        records_ = null;
      } else {
        records_ = null;
        recordsBuilder_ = null;
      }
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return cz.esw.serialization.proto.Measurements.internal_static_esw_PDataset_descriptor;
    }

    @java.lang.Override
    public cz.esw.serialization.proto.PDataset getDefaultInstanceForType() {
      return cz.esw.serialization.proto.PDataset.getDefaultInstance();
    }

    @java.lang.Override
    public cz.esw.serialization.proto.PDataset build() {
      cz.esw.serialization.proto.PDataset result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public cz.esw.serialization.proto.PDataset buildPartial() {
      cz.esw.serialization.proto.PDataset result = new cz.esw.serialization.proto.PDataset(this);
      if (infoBuilder_ == null) {
        result.info_ = info_;
      } else {
        result.info_ = infoBuilder_.build();
      }
      if (recordsBuilder_ == null) {
        result.records_ = records_;
      } else {
        result.records_ = recordsBuilder_.build();
      }
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof cz.esw.serialization.proto.PDataset) {
        return mergeFrom((cz.esw.serialization.proto.PDataset)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(cz.esw.serialization.proto.PDataset other) {
      if (other == cz.esw.serialization.proto.PDataset.getDefaultInstance()) return this;
      if (other.hasInfo()) {
        mergeInfo(other.getInfo());
      }
      if (other.hasRecords()) {
        mergeRecords(other.getRecords());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      cz.esw.serialization.proto.PDataset parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (cz.esw.serialization.proto.PDataset) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private cz.esw.serialization.proto.PInfo info_;
    private com.google.protobuf.SingleFieldBuilderV3<
        cz.esw.serialization.proto.PInfo, cz.esw.serialization.proto.PInfo.Builder, cz.esw.serialization.proto.PInfoOrBuilder> infoBuilder_;
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public boolean hasInfo() {
      return infoBuilder_ != null || info_ != null;
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public cz.esw.serialization.proto.PInfo getInfo() {
      if (infoBuilder_ == null) {
        return info_ == null ? cz.esw.serialization.proto.PInfo.getDefaultInstance() : info_;
      } else {
        return infoBuilder_.getMessage();
      }
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public Builder setInfo(cz.esw.serialization.proto.PInfo value) {
      if (infoBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        info_ = value;
        onChanged();
      } else {
        infoBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public Builder setInfo(
        cz.esw.serialization.proto.PInfo.Builder builderForValue) {
      if (infoBuilder_ == null) {
        info_ = builderForValue.build();
        onChanged();
      } else {
        infoBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public Builder mergeInfo(cz.esw.serialization.proto.PInfo value) {
      if (infoBuilder_ == null) {
        if (info_ != null) {
          info_ =
            cz.esw.serialization.proto.PInfo.newBuilder(info_).mergeFrom(value).buildPartial();
        } else {
          info_ = value;
        }
        onChanged();
      } else {
        infoBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public Builder clearInfo() {
      if (infoBuilder_ == null) {
        info_ = null;
        onChanged();
      } else {
        info_ = null;
        infoBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public cz.esw.serialization.proto.PInfo.Builder getInfoBuilder() {
      
      onChanged();
      return getInfoFieldBuilder().getBuilder();
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    public cz.esw.serialization.proto.PInfoOrBuilder getInfoOrBuilder() {
      if (infoBuilder_ != null) {
        return infoBuilder_.getMessageOrBuilder();
      } else {
        return info_ == null ?
            cz.esw.serialization.proto.PInfo.getDefaultInstance() : info_;
      }
    }
    /**
     * <code>.esw.PInfo info = 1;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        cz.esw.serialization.proto.PInfo, cz.esw.serialization.proto.PInfo.Builder, cz.esw.serialization.proto.PInfoOrBuilder> 
        getInfoFieldBuilder() {
      if (infoBuilder_ == null) {
        infoBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            cz.esw.serialization.proto.PInfo, cz.esw.serialization.proto.PInfo.Builder, cz.esw.serialization.proto.PInfoOrBuilder>(
                getInfo(),
                getParentForChildren(),
                isClean());
        info_ = null;
      }
      return infoBuilder_;
    }

    private cz.esw.serialization.proto.PRecords records_;
    private com.google.protobuf.SingleFieldBuilderV3<
        cz.esw.serialization.proto.PRecords, cz.esw.serialization.proto.PRecords.Builder, cz.esw.serialization.proto.PRecordsOrBuilder> recordsBuilder_;
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public boolean hasRecords() {
      return recordsBuilder_ != null || records_ != null;
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public cz.esw.serialization.proto.PRecords getRecords() {
      if (recordsBuilder_ == null) {
        return records_ == null ? cz.esw.serialization.proto.PRecords.getDefaultInstance() : records_;
      } else {
        return recordsBuilder_.getMessage();
      }
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public Builder setRecords(cz.esw.serialization.proto.PRecords value) {
      if (recordsBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        records_ = value;
        onChanged();
      } else {
        recordsBuilder_.setMessage(value);
      }

      return this;
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public Builder setRecords(
        cz.esw.serialization.proto.PRecords.Builder builderForValue) {
      if (recordsBuilder_ == null) {
        records_ = builderForValue.build();
        onChanged();
      } else {
        recordsBuilder_.setMessage(builderForValue.build());
      }

      return this;
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public Builder mergeRecords(cz.esw.serialization.proto.PRecords value) {
      if (recordsBuilder_ == null) {
        if (records_ != null) {
          records_ =
            cz.esw.serialization.proto.PRecords.newBuilder(records_).mergeFrom(value).buildPartial();
        } else {
          records_ = value;
        }
        onChanged();
      } else {
        recordsBuilder_.mergeFrom(value);
      }

      return this;
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public Builder clearRecords() {
      if (recordsBuilder_ == null) {
        records_ = null;
        onChanged();
      } else {
        records_ = null;
        recordsBuilder_ = null;
      }

      return this;
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public cz.esw.serialization.proto.PRecords.Builder getRecordsBuilder() {
      
      onChanged();
      return getRecordsFieldBuilder().getBuilder();
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    public cz.esw.serialization.proto.PRecordsOrBuilder getRecordsOrBuilder() {
      if (recordsBuilder_ != null) {
        return recordsBuilder_.getMessageOrBuilder();
      } else {
        return records_ == null ?
            cz.esw.serialization.proto.PRecords.getDefaultInstance() : records_;
      }
    }
    /**
     * <code>.esw.PRecords records = 2;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        cz.esw.serialization.proto.PRecords, cz.esw.serialization.proto.PRecords.Builder, cz.esw.serialization.proto.PRecordsOrBuilder> 
        getRecordsFieldBuilder() {
      if (recordsBuilder_ == null) {
        recordsBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            cz.esw.serialization.proto.PRecords, cz.esw.serialization.proto.PRecords.Builder, cz.esw.serialization.proto.PRecordsOrBuilder>(
                getRecords(),
                getParentForChildren(),
                isClean());
        records_ = null;
      }
      return recordsBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:esw.PDataset)
  }

  // @@protoc_insertion_point(class_scope:esw.PDataset)
  private static final cz.esw.serialization.proto.PDataset DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new cz.esw.serialization.proto.PDataset();
  }

  public static cz.esw.serialization.proto.PDataset getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<PDataset>
      PARSER = new com.google.protobuf.AbstractParser<PDataset>() {
    @java.lang.Override
    public PDataset parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new PDataset(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<PDataset> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<PDataset> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public cz.esw.serialization.proto.PDataset getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

