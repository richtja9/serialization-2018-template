package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.ADataset;
import cz.esw.serialization.avro.AInfo;
import cz.esw.serialization.avro.ARecords;
import cz.esw.serialization.avro.Message;
import cz.esw.serialization.json.DataType;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.compress.utils.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

	private final InputStream is;
	private final OutputStream os;
	private Map<Integer, ADataset> datasets;

	public AvroDataHandler(InputStream is, OutputStream os) {
		this.is = is;
		this.os = os;
	}

	@Override
	public void start() {
		this.datasets = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
		ADataset dataset = new ADataset(new AInfo(datasetId, timestamp, measurerName), new ARecords(new ArrayList<Double>(), new ArrayList<Double>(), new ArrayList<Double>()));
		this.datasets.put(datasetId, dataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
		ADataset dataset = this.datasets.get(datasetId);
		if (dataset == null) {
			throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
		}
		switch (type) {
			case PING:
				dataset.getArecord().getPing().add(value);
				break;
			case UPLOAD:
				dataset.getArecord().getUpload().add(value);
				break;
			case DOWNLOAD:
				dataset.getArecord().getDownload().add(value);
				break;
			default:
				throw new ValueException("Unexpected DataType: " + type);
		}
	}

	@Override
	public void getResults(ResultConsumer consumer) throws IOException {
		ArrayList<ADataset> datasets = new ArrayList<ADataset>(this.datasets.values());
		Message message = new Message(datasets);

		DatumWriter<Message> datumWriter = new SpecificDatumWriter<Message>(Message.class);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream , null);
		datumWriter.write(message, encoder);
		encoder.flush();
		int size = byteArrayOutputStream.size();
		os.write(String.format("%d\n", size).getBytes());
		os.flush();
		os.write(byteArrayOutputStream.toByteArray());
		os.flush();
		System.out.println("Message sent...");
		byte[] bytes = IOUtils.toByteArray(is);
		DatumReader<Message> datumreader = new SpecificDatumReader<>(Message.class);
		BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(bytes, null);

		Message new_message = new Message();
		datumreader.read(new_message, decoder);

		for(ADataset dataset : new_message.getDataset()){
			AInfo info = dataset.getAinfo();
			consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
			ARecords records = dataset.getArecord();
			consumer.acceptResult(DataType.DOWNLOAD, records.getDownload().get(0));
			consumer.acceptResult(DataType.UPLOAD, records.getUpload().get(0));
			consumer.acceptResult(DataType.PING, records.getPing().get(0));
		}
	}
}
