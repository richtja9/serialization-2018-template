package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.proto.PDataset;
import cz.esw.serialization.proto.PInfo;
import cz.esw.serialization.proto.PMessage;
import cz.esw.serialization.proto.PRecords;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

    private final InputStream is;
    private final OutputStream os;
    private Map<Integer, PDataset.Builder> datasetBuilders;

    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void start() {
        datasetBuilders = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        PDataset.Builder builder = PDataset.newBuilder();
        builder.setInfo(
                PInfo.newBuilder()
                        .setId(datasetId)
                        .setTimestamp(timestamp)
                        .setMeasurerName(measurerName)
        );
        builder.setRecords(
                PRecords.newBuilder()
        );
        datasetBuilders.put(datasetId, builder);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        PDataset.Builder datasetBuilder = datasetBuilders.get(datasetId);
        if (datasetBuilder == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }

        switch (type) {
            case PING:
                datasetBuilder.getRecordsBuilder().addPing(value);
                break;
            case UPLOAD:
                datasetBuilder.getRecordsBuilder().addUpload(value);
                break;
            case DOWNLOAD:
                datasetBuilder.getRecordsBuilder().addDownload(value);
                break;
            default:
                throw new ValueException("Unexpected DataType: " + type);
        }
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        PMessage.Builder messageBuilder = PMessage.newBuilder();
        for (PDataset.Builder datasetBuilder : datasetBuilders.values()) {
            messageBuilder.addMessageArray(datasetBuilder);
        }
        PMessage message = messageBuilder.build();
        int size = message.getSerializedSize();
        os.write(String.format("%d\n", size).getBytes());
        os.flush();
        message.writeTo(os);
        os.flush();
        System.out.println("Message sent...");
        message = PMessage.parseFrom(is);
        for(PDataset dataset : message.getMessageArrayList()){
            PInfo info = dataset.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
            PRecords records = dataset.getRecords();
            consumer.acceptResult(DataType.DOWNLOAD, records.getDownload(0));
            consumer.acceptResult(DataType.UPLOAD, records.getUpload(0));
            consumer.acceptResult(DataType.PING, records.getPing(0));
        }

    }
}
